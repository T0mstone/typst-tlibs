/// Calculate a dictionary that is implied by a value or a dictionary.
/// 
/// In particular, this takes a mapping from output-keys to input-keys,
/// where the value of the output at an output-key is determined
/// by the value of the input at the first associated input-key that is present in the input.
/// 
/// Additionally, the input can be a single non-dictionary value,
/// which will then be used as the value for all specified output-keys.
#let ImpliedDict(
	val,
	/// (dictionary) The possible aliases for each output key.
	/// Shape: ("output key": ("input key 1 (highest precedence)", "input key 2", ...))
	out_keys: (:),
	/// (function(any -> boolean), none) A filter that is applied to every input value (asserted to return `true`)
	filter: none,
	/// (boolean) Whether to fill output keys that would otherwise be missing from the result with a default value
	use_default: false,
	/// (any) The default value to use when `use_default == true`
	default: none,
) = {
	assert(type(filter) in ("function", "none"), message: "invalid filter")

	if type(val) == "dictionary" {
		let dic = val
		let res = (:)

		for (specific_key, arr) in out_keys {
			for key in arr {
				if key in dic {
					let val = dic.at(key)
					if filter != none {
						assert(filter(val), message: "value rejected by filter")
					}
					res.insert(specific_key, val)
					break
				}
			}
			if specific_key not in res and use_default {
				res.insert(specific_key, default)
			}
		}

		res
	} else {
		if filter != none {
			assert(filter(val), message: "value rejected by filter")
		}

		let res = (:)

		for key in out_keys.keys() {
			res.insert(key, val)
		}

		res
	}
}

#let ImpliedCornerDict = ImpliedDict.with(
	out_keys: (
		top-left: ("top-left", "left", "top", "rest"),
		top-right: ("top-right", "top", "right", "rest"),
		bottom-right: ("bottom-right", "right", "bottom", "rest"),
		bottom-left: ("bottom-left", "left", "bottom", "rest")
	)
)

#let ImpliedSideDict = ImpliedDict.with(
	out_keys: (
		top: ("top", "y", "rest"),
		right: ("right", "x", "rest"),
		bottom: ("bottom", "y", "rest"),
		left: ("left", "x", "rest")
	)
)