#let rgb_clamped(r, g, b) = {
	let r = calc.clamp(r / 100%, 0, 1) * 100%
	let g = calc.clamp(g / 100%, 0, 1) * 100%
	let b = calc.clamp(b / 100%, 0, 1) * 100%
	rgb(r, g, b)
}

#let YCoCg(y, co, cg, clamp: true) = {
	assert(0% <= y and y <= 100%, message: "Y out of bounds")
	assert(-50% <= co and co <= 50%, message: "Co out of bounds")
	assert(-50% <= cg and cg <= 50%, message: "Cg out of bounds")
	let tmp = y - cg
	let rgb = if clamp { rgb_clamped } else { rgb }
	rgb(
		tmp + co,
		y + cg,
		tmp - co
	)
}